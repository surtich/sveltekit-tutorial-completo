import type { HandleClientError } from '@sveltejs/kit';

export const handleError: HandleClientError = () => {
	return {
		message: 'An unexpected error has ocurred!',
		code: 'UNEXPECTED'
	};
};
