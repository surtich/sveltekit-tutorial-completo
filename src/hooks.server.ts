import type { Handle, HandleServerError } from '@sveltejs/kit';

// se ejecuta cada vez que el servidor recibe una petición
export const handle: Handle = async ({ event, resolve }) => {
	const { locals, cookies } = event;
	const token = cookies.get('token');
	// se obtiene el usuario de token. aquí lo simulamos.
	locals.user = token ? { name: 'pepe', id: 1 } : undefined;

	const response = await resolve(event);

	return response;
};

export const handleError: HandleServerError = () => {
	return {
		message: 'An unexpected error has ocurred!',
		code: 'UNEXPECTED'
	};
};
