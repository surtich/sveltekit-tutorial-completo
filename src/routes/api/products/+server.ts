import { error, json } from '@sveltejs/kit';
import type { RequestHandler } from './$types';

export const GET: RequestHandler = async ({ fetch }) => {
	const response = await fetch('https://dummyjson.com/products');

	if (response.ok) {
		const resJSON = await response.json();
		return json(resJSON, {
			status: 200
		});
	}

	throw error(response.status, response.statusText);
};

/*
curl --location 'http://localhost:5173/api/products' \
--header 'Content-Type: text/plain' \
--data '{
    "title": "some product"
}'
*/

export const POST: RequestHandler = async ({ request }) => {
	const product = await request.json();

	if (!product.title) {
		throw error(400, 'The product title is required!');
	}
	return json(
		{ id: 123, title: product.title },
		{
			status: 200
		}
	);
};
