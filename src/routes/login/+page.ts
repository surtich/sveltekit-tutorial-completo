import { redirect } from '@sveltejs/kit';
import type { PageLoad } from '../products/$types';

export const load: PageLoad = async ({ parent }) => {
	const { user } = await parent();

	if (user) {
		throw redirect(307 /*temporal redirect */, '/');
	}
};
