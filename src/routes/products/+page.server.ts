import { error } from '@sveltejs/kit';
import type { PageServerLoad } from './$types';

export const load: PageServerLoad = async ({ fetch }) => {
	// La función fetch de SvelteKit, a diferencia del navegador, permite rutas relativas.
	// Realmente no se va a hacer una llamada de red sino que se llama directamente a la función GET de +server.ts
	// Se podría haber hecho directamente la llamada a dummyjson desde aquí, pero
	// esto permite que +server.ts pasara credenciales,...
	const response = await fetch('api/products');

	if (response.ok) {
		return {
			// SvelteKit permite omitir el await
			products: response.json()
		};
	}

	throw error(response.status, response.statusText);
};
