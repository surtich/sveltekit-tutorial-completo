import type { PageLoad } from './$types';

export const load: PageLoad = async ({ data, depends }) => {
	console.log('load from products/+page.server.ts');
	depends('app:products');

	return {
		products: data.products,
		title: 'Products list'
	};
};

export const ssr = true;
export const csr = false;
