import adapter from '@sveltejs/adapter-auto';
import sveltePreprocess from 'svelte-preprocess';
import { vitePreprocess } from '@sveltejs/kit/vite';

/** @type {import('@sveltejs/kit').Config} */
const config = {
	// Consult https://kit.svelte.dev/docs/integrations#preprocessors
	// for more information about preprocessors
	preprocess: [sveltePreprocess(), vitePreprocess()],

	kit: {
		// adapter-auto only supports some environments, see https://kit.svelte.dev/docs/adapter-auto for a list.
		// If your environment is not supported or you settled on a specific environment, switch out the adapter.
		// See https://kit.svelte.dev/docs/adapters for more information about adapters.
		adapter: adapter(),
		alias: {
			$components: 'src/lib/components'
		},
		csrf: {
			checkOrigin: false
		}
		/* Las variables públicas se pueden acceder en el cliente y en el servidor
		y son por defecto las que empiezan por PUBLIC_ (se puede cambiar aquí)
		el resto se consideran privadas y sólo se accesibles desde el servidor.
		env: {
			publicPrefix: "PUBLIC_"
		}
		*/
	}
};

export default config;
